<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $url
 * @property int|null $author_id
 * @property int|null $project_id
 *
 * @property Projects $project
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['title', 'url'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Имя',
            'url' => 'Год рождения',
            'sort' => 'Кол-во книг'

        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\ProjectsQuery
     */
    public function getBook()
    {
        return $this->hasMany(Books::className(), ['id' => 'author_id']);
    }

    public function getBookscount()
    {
        return $this->hasMany(Books::className(), ['author_id' => 'id'])->count();
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\FilesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AuthorsQuery(get_called_class());
    }
}
