<?php

namespace app\models;

use Yii;
use yii\helpers\Json;


class Books extends \yii\db\ActiveRecord
{
    public $xml_file;
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['active', 'sort', 'author_id'], 'integer'],
            [['skills'], 'safe'],
            [['updated_at', 'created_at'], 'safe'],
            [['title', 'organization', 'role', 'link', 'type', 'start', 'end'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'author_id' => 'Автор'

        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }


    public static function find()
    {
        return new \app\models\query\BooksQuery(get_called_class());
    }


}
