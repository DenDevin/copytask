<?php

use yii\db\Migration;

/**
 * Class m200201_203621_add_profile_avatar_column
 */
class m200201_203621_add_profile_avatar_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'avatar', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profile', 'avatar');


    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_203621_add_profile_avatar_column cannot be reverted.\n";

        return false;
    }
    */
}
