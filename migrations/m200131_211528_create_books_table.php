<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m200131_211528_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'organization' => $this->string(),
            'start' => $this->integer()->null(),
            'end' => $this->integer()->null(),
            'role' => $this->string(),
            'link' => $this->string(),
            'skills' => $this->text(),
            'attachments' => $this->text(),
            'type' => $this->string(),
            'active' => $this->boolean(),
            'sort' => $this->integer(),
            'author_id' => $this->integer(),
            'updated_at' => $this->timestamp()->defaultValue(null),
            'created_at' => $this->timestamp(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
